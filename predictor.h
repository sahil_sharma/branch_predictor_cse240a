#ifndef PREDICTOR_H
#define PREDICTOR_H

#include <stdio.h>
#include <stdlib.h>

// Make changes here to update budget size
//#define BUDGET_8k
//#define BUDGET_16k
//#define BUDGET_32k
//#define BUDGET_64k
//#define BUDGET_128k
#define BUDGET_1M


/*
  Define all your tables and their sizes here.
  All tables must be statically-sized.
  Please don't call malloc () or any of its relatives within your
  program.  The predictor will have a budget, namely (32K + 256) bits
  (not bytes).  That encompasses all storage (all tables, plus GHR, if
  necessary -- but not PC) used for the predictor.  That may not be
  the amount of storage your predictor uses, however -- for example,
  you may implement 2-bit predictors with a table of ints, in which
  case the simulator will use more memory -- that's okay, we're only
  concerned about the memory used by the simulated branch predictor.
*/
#ifdef BUDGET_8k
// Two Level Predictor
#define TLLP_PBHT_SIZE 10
#define TLLP_PBHT_BITS 7
#define TLLP_GPHT_BITS 2

// Alpha Predictor
#define ALPHA_LHT_SIZE 9
#define ALPHA_LHT_BITS 8
#define ALPHA_LP_BITS 2
#define ALPHA_PH_BITS 9
#define ALPHA_GP_BITS 2
#define ALPHA_CP_BITS 4

#define GHR_BITS_GSHARE 12
//Perceptron GHR: LHR = 4:8, (4,8]:7, (8,16]:6, (16,32]:5, 64:4
#define GHR_BITS_PERCEPTRON 16
#define LHR_BITS_PERCEPTRON 6

#endif

#ifdef BUDGET_16k
// Two Level Predictor
#define TLLP_PBHT_SIZE 10
#define TLLP_PBHT_BITS 11
#define TLLP_GPHT_BITS 2

// Alpha Predictor
#define ALPHA_LHT_SIZE 10
#define ALPHA_LHT_BITS 9
#define ALPHA_LP_BITS 2
#define ALPHA_PH_BITS 10
#define ALPHA_GP_BITS 2
#define ALPHA_CP_BITS 4

#define GHR_BITS_GSHARE 13
//Perceptron GHR: LHR = 4:9, 8:8, 16:7, 32:6, 64:5
#define GHR_BITS_PERCEPTRON 32
#define LHR_BITS_PERCEPTRON 6
#endif

#ifdef BUDGET_32k
// Two Level Predictor
#define TLLP_PBHT_SIZE 11
#define TLLP_PBHT_BITS 11
#define TLLP_GPHT_BITS 2

// Alpha Predictor
#define ALPHA_LHT_SIZE 11
#define ALPHA_LHT_BITS 11
#define ALPHA_LP_BITS 2
#define ALPHA_PH_BITS 10
#define ALPHA_GP_BITS 2
#define ALPHA_CP_BITS 4

#define GHR_BITS_GSHARE 14
//Perceptron GHR: LHR = 4:10, 8:9, 16:8, 32:7, 64:6
#define GHR_BITS_PERCEPTRON 16
#define LHR_BITS_PERCEPTRON 8
#endif

#ifdef BUDGET_64k
// Two Level Predictor
#define TLLP_PBHT_SIZE 12
#define TLLP_PBHT_BITS 12
#define TLLP_GPHT_BITS 2


// Alpha Predictor
#define ALPHA_LHT_SIZE 12
#define ALPHA_LHT_BITS 12
#define ALPHA_LP_BITS 2
#define ALPHA_PH_BITS 10
#define ALPHA_GP_BITS 2
#define ALPHA_CP_BITS 4

//Perceptron GHR: LHR = 4:11, 8:10, 16:9, 32:8, 64:7
#define GHR_BITS_GSHARE 15
#define GHR_BITS_PERCEPTRON 32
#define LHR_BITS_PERCEPTRON 8
#endif

#ifdef BUDGET_128k
// Two Level Predictor
#define TLLP_PBHT_SIZE 13
#define TLLP_PBHT_BITS 13
#define TLLP_GPHT_BITS 2

// Alpha Predictor
#define ALPHA_LHT_SIZE 13
#define ALPHA_LHT_BITS 13
#define ALPHA_LP_BITS 2
#define ALPHA_PH_BITS 10
#define ALPHA_GP_BITS 2
#define ALPHA_CP_BITS 4

#define GHR_BITS_GSHARE 16
//Perceptron GHR: LHR = 4:12, 8:11, 16:10, 32:9, 64:8
#define GHR_BITS_PERCEPTRON 32
#define LHR_BITS_PERCEPTRON 9
#endif

#ifdef BUDGET_1M
// Two Level Predictor
#define TLLP_PBHT_SIZE 16
#define TLLP_PBHT_BITS 17
#define TLLP_GPHT_BITS 2

// Alpha Predictor
#define ALPHA_LHT_SIZE 15
#define ALPHA_LHT_BITS 17
#define ALPHA_LP_BITS 2
#define ALPHA_PH_BITS 14
#define ALPHA_GP_BITS 2
#define ALPHA_CP_BITS 5

#define GHR_BITS_GSHARE 19
//Perceptron GHR: LHR = 4:15, 8:14, 16:13, 32:12, 64:11
#define GHR_BITS_PERCEPTRON 4
#define LHR_BITS_PERCEPTRON 15
#endif

#define MODE_TWO_LEVEL 1
#define MODE_ALPHA 2
#define MODE_PERCEPTRON 3
#define MODE_GSHARE 4

extern int GShare_PHT[(1<<GHR_BITS_GSHARE)];
extern long long int GHR;
extern int mode;

extern char perceptron_table[(1<<LHR_BITS_PERCEPTRON)][GHR_BITS_PERCEPTRON];
extern int threshold;

// Variables for two level predictor
extern int PBHT[1<<TLLP_PBHT_SIZE];
extern int GPHT[1<<TLLP_PBHT_BITS];

// Variables for Alpha Predictor
extern int LocalHistoryTable[1<<ALPHA_LHT_SIZE];
extern int LocalPrediction[1<<ALPHA_LHT_BITS];
extern int GlobalPrediction[1<<ALPHA_PH_BITS];
extern int PathChoice[1<<ALPHA_PH_BITS];
extern int PathHistory;

/*
  Initialize the predictor.
*/
void init_predictor ();

/*
  Make a prediction for conditional branch instruction at PC 'pc'.
  Returning true indicates a prediction of taken; returning false
  indicates a prediction of not taken.
*/
bool make_prediction (unsigned int pc);
bool gshare_predict(unsigned int PC);
/*
  Train the predictor the last executed branch at PC 'pc' and with
  outcome 'outcome' (true indicates that the branch was taken, false
  indicates that the branch was not taken).
*/
void train_predictor (unsigned int pc, bool outcome);
void gshare_train(unsigned int PC, bool outcome);

bool alpha_predict(unsigned int PC);
void alpha_train(unsigned int PC, bool outcome);
bool tllp_predict(unsigned int PC);
void tllp_train(unsigned int PC, bool outcome);
bool perceptron_predict(unsigned int PC);
void perceptron_train(unsigned int PC, bool outcome);
#endif
